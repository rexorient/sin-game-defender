/* SINGameDefender.cs

Copyright 2013 RexOrient -- http://www.sinbfclan.com

Permission is hereby granted, free of charge, to any person or organization
obtaining a copy of the software and accompanying documentation covered by
this license (the "Software") to use, reproduce, display, distribute,
execute, and transmit the Software, and to prepare derivative works of the
Software, and to permit third-parties to whom the Software is furnished to
do so, without restriction.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

 */

using System;
using System.IO;
using System.Text;
using System.Reflection;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Runtime.InteropServices;
using System.Xml;

using PRoCon.Core;
using PRoCon.Core.Plugin;
using PRoCon.Core.Plugin.Commands;
using PRoCon.Core.Players;
using PRoCon.Core.Players.Items;
using PRoCon.Core.Battlemap;
using PRoCon.Core.Maps;

namespace PRoConEvents
{
    using EventType = PRoCon.Core.Events.EventType;
    using CapturableEvent = PRoCon.Core.Events.CapturableEvents;

    /* Configuration Class */
    public class SGDConfiguration
    {
        public int debugLevel;

        // Data acquisition parameters
        public int ignoreSecondsAfterJoin;
        public int ignoreSecondsAfterMapChange;
        public int pingWindowSize;

        // Offense handling parameters
        public int kickSecondsAfterOffense;
        public bool enableOffenseMessage;
        public bool enableAnnounceMessage;
        public bool whiteListReservedSlots;
        public List<string> whiteList;

        // High Ping
        public bool enableHighPingOffense;
        public int pingValueForKick;
        public string highPingOffenseMessage;
        public string highPingKickMessage;
        public string highPingAnnounceMessage;

        // No Ping
        public bool enableNoPingOffense;
        public string noPingOffenseMessage;
        public string noPingKickMessage;
        public string noPingAnnounceMessage;
    }


    public class SGDPlayerInfo
    {
        /* Enums */
        public enum Offense { None, HighPing, NoPing };

        private SINGameDefender api;
        private SGDConfiguration config;

        public string name;

        public LinkedList<int> data;
        public DateTime collectAfter;
        public DateTime kickAfter;

        private int averagePing = 0;
        private int lastPing = 0;

        public Offense offense = Offense.None;

        public SGDPlayerInfo(SINGameDefender api, SGDConfiguration config, string name)
        {
            this.api = api;
            this.config = config;

            this.name = name;

            data = new LinkedList<int>();
            collectAfter = DateTime.Now.AddSeconds(config.ignoreSecondsAfterJoin);
            kickAfter = DateTime.MaxValue;
        }

        public void addPing(int Ping)
        {
            DebugMessage(2, "addPing(" + Ping + ")");

            lastPing = Ping;

            // Are we still ignoring pings for this player?
            if (DateTime.Now < collectAfter)
            {
                TimeSpan secondsLeft = collectAfter - DateTime.Now;
                DebugMessage(3, "Ignoring - remaining: " + secondsLeft);
                return;
            }

            // Update ping data
            data.AddLast(Ping);
            while (data.Count > config.pingWindowSize)
            {
                data.RemoveFirst();
            }

            // If white listed, bail
            if (config.whiteList.Contains(name))
            {
                DebugMessage(3, "Ignoring - WhiteListed");
                return;
            }

            // If white listed in reserved slots, bail
            if(config.whiteListReservedSlots && api.reservedSlots.Contains(name))
            {
                DebugMessage(3, "Ignoring - WhiteListed via reserved slots");
                return;
            }

            // Handle invalid ping
            if (Ping <= 0 || Ping > 10000)
            {
                if (config.enableNoPingOffense)
                {
                    MarkForKicking(SGDPlayerInfo.Offense.NoPing, config.noPingOffenseMessage);
                    return;
                }

                // Bail -- we can't calculate an average ping using invalid data.
                DebugMessage(3, "Ignoring - invalid ping value!");
                return;
            }

            // If not enough data to calculate average, bail
            if (data.Count < config.pingWindowSize)
            {
                DebugMessage(3, "Ignoring - Not enough data - " + data.Count + "/" + config.pingWindowSize);
                return;
            }

            // Calculate Average
            averagePing = 0;
            foreach (int ping in data)
            {
                averagePing += ping;
            }
            averagePing = averagePing / data.Count;
            DebugMessage(2, "Calculate Average Ping: " + averagePing);

            // High Ping check
            if (config.enableHighPingOffense)
            {
                if (averagePing > config.pingValueForKick)
                {
                    MarkForKicking(SGDPlayerInfo.Offense.HighPing, config.highPingOffenseMessage);
                    return;
                }
            }

            // Everything is ok, make sure not to kick the player.
            kickAfter = DateTime.MaxValue;
            offense = Offense.None;
        }

        private void MarkForKicking(SGDPlayerInfo.Offense offense, string message)
        {
            if (kickAfter.Equals(DateTime.MaxValue))
            {
                this.offense = offense;
                kickAfter = DateTime.Now.AddSeconds(config.kickSecondsAfterOffense);
                DebugMessage(1, ExpandMessage("Marked for kicking after %markedTime%.  Reason=%offense%  AveragePing=%avgPing%  LastPing=%lastPing%  MaxPing=%maxPing%"));

                // Optionally notify player
                if (config.enableOffenseMessage)
                {
                    api.ExecuteCommand("procon.protected.send", "admin.say", ExpandMessage(message), "player", name);
                    DebugMessage(2, ExpandMessage("Sent private message: " + ExpandMessage(message)));
                }
            }
            else
            {
                DebugMessage(3, "Ignoring - Already marked for kicking at " + kickAfter);
            }
        }

        public void TimedCheck()
        {
            if (kickAfter < DateTime.Now)
            {
                string kickMessage = "";
                string announceMessage = "";
                switch (offense)
                {
                    case Offense.HighPing:
                        kickMessage = config.highPingKickMessage;
                        announceMessage = config.highPingAnnounceMessage;
                        break;

                    case Offense.NoPing:
                        kickMessage = config.noPingKickMessage;
                        announceMessage = config.noPingAnnounceMessage;
                        break;
                }

                // Kick
                api.ExecuteCommand("procon.protected.send", "admin.kickPlayer", name, ExpandMessage(kickMessage));
                DebugMessage(1, ExpandMessage("Kicked with message: " + ExpandMessage(kickMessage)));

                // Optionally announce
                if(config.enableAnnounceMessage)
                {
                    api.ExecuteCommand("procon.protected.send", "admin.say", ExpandMessage(announceMessage), "all");
                    DebugMessage(2, ExpandMessage("Announced kick: " + ExpandMessage(announceMessage)));
                }
            }
        }

        public string ExpandMessage(string message)
        {
            return message
                .Replace("%name%", name)
                .Replace("%offense%", offense.ToString())
                .Replace("%lastPing%", lastPing.ToString())
                .Replace("%avgPing%", averagePing.ToString())
                .Replace("%maxPing%", config.pingValueForKick.ToString())
                .Replace("%markedTime%", kickAfter.ToString())
                .Replace("%kickSeconds%", config.kickSecondsAfterOffense.ToString());
        }

        private void DebugMessage(int level, string strMessage)
        {
            if (level <= config.debugLevel)
            {
                api.ExecuteCommand("procon.protected.pluginconsole.write", "^bSIN Game Defender for " + name + ": " + strMessage);
            }
        }
    }

    public class SINGameDefender : PRoConPluginAPI, IPRoConPluginInterface
    {
        public enum GameVersion { BF3, BF4 };
        
        private Dictionary<string, SGDPlayerInfo> playerMap;
        private SGDConfiguration config;
        private GameVersion gameVersion = GameVersion.BF3;
        public List<String> reservedSlots = new List<String>();

        public SINGameDefender()
        {
            playerMap = new Dictionary<string, SGDPlayerInfo>();

            config = new SGDConfiguration();
            config.debugLevel = 1;

            // Data acquisition parameters
            config.ignoreSecondsAfterJoin = 60;
            config.ignoreSecondsAfterMapChange = 30;
            config.pingWindowSize = 4;

            // Offense handling parameters
            config.kickSecondsAfterOffense = 30;
            config.enableOffenseMessage = true;
            config.enableAnnounceMessage = true;
            config.whiteListReservedSlots = true;
            config.whiteList = new List<string>();

            // High Ping
            config.enableHighPingOffense = true;
            config.pingValueForKick = 200;
            config.highPingOffenseMessage = "Your ping is too high, you will be kicked in %kickSeconds% seconds!";
            config.highPingKickMessage = "Ping too high (%avgPing%)!";
            config.highPingAnnounceMessage = "Player %name% kicked for high ping!";

            // No Ping
            config.enableNoPingOffense = false;
            config.noPingOffenseMessage = "Your have no ping, you will be kicked in %kickSeconds% seconds!";
            config.noPingKickMessage = "No ping!";
            config.noPingAnnounceMessage = "Player %name% kicked for no ping!";
        }

        public string GetPluginName()
        {
            return "SIN Game Defender";
        }

        public string GetPluginVersion()
        {
            return "0.4";
        }

        public string GetPluginAuthor()
        {
            return "RexOrient";
        }

        public string GetPluginWebsite()
        {
            return "www.sinbfclan.com";
        }

        public string GetPluginDescription()
        {
            return @"
<h2>Description</h2>
<p>The SIN Game Defender plugin provides a powerful and fair mechanism for kicking players with high or no pings.</p>

<h2>Settings</h2>
    <h3>Message substritutions</h3>
        <ul>
            <li><b>%name%</b> - Player name.</li>
            <li><b>%offense%</b> - One of 'HighPing' or 'NoPing' (meant for internal use.)</li>
            <li><b>%lastPing%</b> - Last observed ping time in seconds.</li>
            <li><b>%avgPing%</b> - Current average ping time in seconds.</li>
            <li><b>%maxPing%</b> - Ping value at or above which the player gets kicked.</li>
            <li><b>%markedTime%</b> - Date/Time after which the player will be kicked (only set once the player has been marked for kicking.)</li>
            <li><b>%kickSeconds%</b> - Number of seconds after which a player will be kicked when he is marked for kicking.</li>
        </ul>

    <h3>Debug levels</h3>
        <ul>
            <li><b>0</b> - No debug (only errors are reported)</li>
            <li><b>1</b> - Info</li>
            <li><b>2</b> - Debug</li>
            <li><b>3</b> - Trace</li>
        </ul>

";
        }

        public void OnPluginLoaded(string strHostName, string strPort, string strPRoConVersion)
        {
            this.RegisterEvents(this.GetType().Name,
                "OnPlayerLeft",
                "OnPlayerPingedByAdmin",
                "OnListPlayers",
                "OnRoundOver",
                "OnLevelLoaded",
                "OnReservedSlotsList"
                );
        }

        public void OnPluginEnable()
        {
            this.ExecuteCommand("procon.protected.tasks.remove", "SGDHeartBeat");
            this.ExecuteCommand("procon.protected.tasks.add", "SGDHeartBeat", "1", "10", "-1", "procon.protected.plugins.call", "SINGameDefender", "SGDHeartBeat");
            this.ExecuteCommand("procon.protected.pluginconsole.write", "^b" + GetPluginName() + " " + GetPluginVersion() + "^2Enabled!");
        }

        public void OnPluginDisable()
        {
            this.ExecuteCommand("procon.protected.tasks.remove", "SGDHeartBeat");
            this.ExecuteCommand("procon.protected.pluginconsole.write", "^b" + GetPluginName() + " " + GetPluginVersion() + "^2Disabled!");
            playerMap.Clear();
        }

        public List<CPluginVariable> GetDisplayPluginVariables()
        {
            List<CPluginVariable> lstReturn = new List<CPluginVariable>();

            // Data acquisition parameters
            lstReturn.Add(new CPluginVariable("Data Acquisition|Ignore seconds after join", typeof(int), config.ignoreSecondsAfterJoin));
            lstReturn.Add(new CPluginVariable("Data Acquisition|Ignore seconds after map change", typeof(int), config.ignoreSecondsAfterMapChange));
            lstReturn.Add(new CPluginVariable("Data Acquisition|Pings to average", typeof(int), config.pingWindowSize));

            // Offense handling parameters
            lstReturn.Add(new CPluginVariable("Offense Handling|Kick seconds after offense", typeof(int), config.kickSecondsAfterOffense));
            lstReturn.Add(new CPluginVariable("Offense Handling|Private message player upon offense?", typeof(enumBoolYesNo), config.enableOffenseMessage ? enumBoolYesNo.Yes : enumBoolYesNo.No));
            lstReturn.Add(new CPluginVariable("Offense Handling|Announce kick to other players?", typeof(enumBoolYesNo), config.enableAnnounceMessage ? enumBoolYesNo.Yes : enumBoolYesNo.No));
            lstReturn.Add(new CPluginVariable("Offense Handling|Whitelist reserved slots?", typeof(enumBoolYesNo), config.whiteListReservedSlots ? enumBoolYesNo.Yes : enumBoolYesNo.No));
            lstReturn.Add(new CPluginVariable("Offense Handling|Whitelist", typeof(string[]), config.whiteList.ToArray()));

            // High Ping
            lstReturn.Add(new CPluginVariable("High Ping|Enable high ping offense?", typeof(enumBoolYesNo), config.enableHighPingOffense ? enumBoolYesNo.Yes : enumBoolYesNo.No));
            if(config.enableHighPingOffense)
            {
                lstReturn.Add(new CPluginVariable("High Ping|Maximum ping", typeof(int), config.pingValueForKick));
                if (config.enableOffenseMessage)
                {
                    lstReturn.Add(new CPluginVariable("High Ping|High Ping private message on offense", typeof(string), config.highPingOffenseMessage));
                }
                lstReturn.Add(new CPluginVariable("High Ping|High Ping kick message", typeof(string), config.highPingKickMessage));
                if (config.enableAnnounceMessage)
                {
                    lstReturn.Add(new CPluginVariable("High Ping|High Ping announce message", typeof(string), config.highPingAnnounceMessage));
                }
            }

            // No Ping
            lstReturn.Add(new CPluginVariable("No Ping|Enable no ping offense?", typeof(enumBoolYesNo), config.enableNoPingOffense ? enumBoolYesNo.Yes : enumBoolYesNo.No));
            if (config.enableNoPingOffense)
            {
                if (config.enableOffenseMessage)
                {
                    lstReturn.Add(new CPluginVariable("No Ping|No Ping private message on offense", typeof(string), config.noPingOffenseMessage));
                }
                lstReturn.Add(new CPluginVariable("No Ping|No Ping kick message", typeof(string), config.noPingKickMessage));
                if (config.enableAnnounceMessage)
                {
                    lstReturn.Add(new CPluginVariable("No Ping|No Ping announce message", typeof(string), config.noPingAnnounceMessage));
                }
            }

            lstReturn.Add(new CPluginVariable("Other|Debug level (0-3)", typeof(int), config.debugLevel));
            return lstReturn;
        }

        public List<CPluginVariable> GetPluginVariables()
        {
            List<CPluginVariable> lstReturn = new List<CPluginVariable>();

            // Data acquisition parameters
            lstReturn.Add(new CPluginVariable("Data Acquisition|Ignore seconds after join", typeof(int), config.ignoreSecondsAfterJoin));
            lstReturn.Add(new CPluginVariable("Data Acquisition|Ignore seconds after map change", typeof(int), config.ignoreSecondsAfterMapChange));
            lstReturn.Add(new CPluginVariable("Data Acquisition|Pings to average", typeof(int), config.pingWindowSize));

            // Offense handling parameters
            lstReturn.Add(new CPluginVariable("Offense Handling|Kick seconds after offense", typeof(int), config.kickSecondsAfterOffense));
            lstReturn.Add(new CPluginVariable("Offense Handling|Private message player upon offense?", typeof(enumBoolYesNo), config.enableOffenseMessage ? enumBoolYesNo.Yes : enumBoolYesNo.No));
            lstReturn.Add(new CPluginVariable("Offense Handling|Announce kick to other players?", typeof(enumBoolYesNo), config.enableAnnounceMessage ? enumBoolYesNo.Yes : enumBoolYesNo.No));
            lstReturn.Add(new CPluginVariable("Offense Handling|Whitelist reserved slots?", typeof(enumBoolYesNo), config.whiteListReservedSlots ? enumBoolYesNo.Yes : enumBoolYesNo.No));
            lstReturn.Add(new CPluginVariable("Offense Handling|Whitelist", typeof(string[]), config.whiteList.ToArray()));

            // High Ping
            lstReturn.Add(new CPluginVariable("High Ping|Enable high ping offense?", typeof(enumBoolYesNo), config.enableHighPingOffense ? enumBoolYesNo.Yes : enumBoolYesNo.No));
            lstReturn.Add(new CPluginVariable("High Ping|Maximum ping", typeof(int), config.pingValueForKick));
            lstReturn.Add(new CPluginVariable("High Ping|High Ping private message on offense", typeof(string), config.highPingOffenseMessage));
            lstReturn.Add(new CPluginVariable("High Ping|High Ping kick message", typeof(string), config.highPingKickMessage));
            lstReturn.Add(new CPluginVariable("High Ping|High Ping announce message", typeof(string), config.highPingAnnounceMessage));

            // No Ping
            lstReturn.Add(new CPluginVariable("No Ping|Enable no ping offense?", typeof(enumBoolYesNo), config.enableNoPingOffense ? enumBoolYesNo.Yes : enumBoolYesNo.No));
            lstReturn.Add(new CPluginVariable("No Ping|No Ping private message on offense", typeof(string), config.noPingOffenseMessage));
            lstReturn.Add(new CPluginVariable("No Ping|No Ping kick message", typeof(string), config.noPingKickMessage));
            lstReturn.Add(new CPluginVariable("No Ping|No Ping announce message", typeof(string), config.noPingAnnounceMessage));

            lstReturn.Add(new CPluginVariable("Other|Debug level (0-3)", typeof(int), config.debugLevel));
            return lstReturn;
        }


        public void SetPluginVariable(string strVariable, string strValue)
        {
            HandleBooleanConfig(strVariable, strValue, "Private message player upon offense?", ref config.enableOffenseMessage);
            HandleBooleanConfig(strVariable, strValue, "Announce kick to other players?", ref config.enableAnnounceMessage);
            HandleBooleanConfig(strVariable, strValue, "Enable high ping offense?", ref config.enableHighPingOffense);
            HandleBooleanConfig(strVariable, strValue, "Enable no ping offense?", ref config.enableNoPingOffense);
            HandleBooleanConfig(strVariable, strValue, "Whitelist reserved slots?", ref config.whiteListReservedSlots);

            HandleStringConfig(strVariable, strValue, "High Ping private message on offense", ref config.highPingOffenseMessage);
            HandleStringConfig(strVariable, strValue, "High Ping kick message", ref config.highPingKickMessage);
            HandleStringConfig(strVariable, strValue, "High Ping announce message", ref config.highPingAnnounceMessage);
            HandleStringConfig(strVariable, strValue, "No Ping private message on offense", ref config.noPingOffenseMessage);
            HandleStringConfig(strVariable, strValue, "No Ping kick message", ref config.noPingKickMessage);
            HandleStringConfig(strVariable, strValue, "No Ping announce message", ref config.noPingAnnounceMessage);

            HandleIntegerConfig(strVariable, strValue, "Ignore seconds after join", ref config.ignoreSecondsAfterJoin, 0, 120);
            HandleIntegerConfig(strVariable, strValue, "Ignore seconds after map change", ref config.ignoreSecondsAfterMapChange, 0, 120);
            HandleIntegerConfig(strVariable, strValue, "Pings to average", ref config.pingWindowSize, 1, 20);
            HandleIntegerConfig(strVariable, strValue, "Kick seconds after offense", ref config.kickSecondsAfterOffense, 0, 300);
            HandleIntegerConfig(strVariable, strValue, "Debug level (0-3)", ref config.debugLevel, 0, 3);
            HandleIntegerConfig(strVariable, strValue, "Maximum ping", ref config.pingValueForKick, 25, 400);

            HandleListConfig(strVariable, strValue, "Whitelist", ref config.whiteList);
        }

        private bool HandleBooleanConfig(string strVariable, string strValue, string name, ref bool value)
        {
            if (!strVariable.Equals(name))
                return false;
            value = strValue.Equals("Yes") || strValue.Equals("true") || strValue.Equals("On");
            return true;
        }

        private bool HandleStringConfig(string strVariable, string strValue, string name, ref string value)
        {
            if (!strVariable.Equals(name))
                return false;
            value = strValue;
            return true;
        }

        private bool HandleIntegerConfig(string strVariable, string strValue, string name, ref int value, int min, int max)
        {
            if (!strVariable.Equals(name))
                return false;

            int iValue = 0;
            if(int.TryParse(strValue, out iValue) && iValue >= min && iValue <= max)
                value = iValue;
            else
                DebugMessage(1, "Entered value (" + strValue + ") for '" + name + "' exceeds min or max (" + min + " - " + max + ")!");
            return true;
        }

        private bool HandleListConfig(string strVariable, string strValue, string name, ref List<string> value)
        {
            if (!strVariable.Equals(name))
                return false;
            value = new List<string>(CPluginVariable.DecodeStringArray(strValue));
            return true;
        }


        public override void OnPlayerLeft(CPlayerInfo cpiPlayer)
        {
            DebugMessage(3, "OnPlayerLeft(" + cpiPlayer.SoldierName + ")");
            RemoveSoldier(cpiPlayer.SoldierName);
        }


        public override void OnListPlayers(List<CPlayerInfo> lstPlayers, CPlayerSubset cpsSubset)
        {
            DebugMessage(3, "OnListPlayers(" + lstPlayers.Count + ")");
            if ((lstPlayers != null))
            {
                List<string> activePlayers = new List<string>();
                foreach (CPlayerInfo playerInfo in lstPlayers)
                {
                    string name = playerInfo.SoldierName;
                    int ping = playerInfo.Ping;

                    activePlayers.Add(name);

                    SGDPlayerInfo info = GetOrAddPlayerInfo(name);

                    // in BF3 this is always 0
                    if(gameVersion == GameVersion.BF4)
                        info.addPing(playerInfo.Ping);
                }

                // Remove players which left the server
                List<string> inactivePlayers = playerMap.Keys.Except(activePlayers).ToList();
                foreach (string name in inactivePlayers)
                {
                    RemoveSoldier(name);
                }
            }
        }


        public override void OnPlayerPingedByAdmin(string soldierName, int ping)
        {
            DebugMessage(3, "OnPlayerPingedByAdmin(" + soldierName + "," + ping + ")");

            SGDPlayerInfo info = GetOrAddPlayerInfo(soldierName);
            info.addPing(ping);
        }


        public void OnPluginLoadingEnv(List<string> lstPluginEnv)
        {
            foreach (String env in lstPluginEnv)
            {
                DebugMessage(3, "OnPluginLoadingEnv(" + env + ")");
            }
            switch (lstPluginEnv[1])
            {
                case "BF3": gameVersion = GameVersion.BF3; break;
                case "BF4": gameVersion = GameVersion.BF4; break;
                default: break;
            }
            DebugMessage(1, "Game Version " + gameVersion);
        }

        public override void OnRoundOver(int winningTeamId)
        {
            DebugMessage(3, "OnRoundOver(" + winningTeamId + ")");

            // Ignore pings for 3 minutes -- this should cover the time people get their score and then some.
            SetIgnorePings(180);
        }

        public override void OnReservedSlotsList(List<String> lstSoldierNames)
        {
            DebugMessage(3, "OnReservedSlotsList(" + lstSoldierNames.Count + ")");
            reservedSlots = new List<String>(lstSoldierNames);
        }

        public override void OnLevelLoaded(String mapFileName, String Gamemode, int roundsPlayed, int roundsTotal)
        {
            DebugMessage(3, "OnLevelLoaded(" + mapFileName + "," + Gamemode + "," + roundsPlayed + "," + roundsTotal + ")");

            // Ignore pings for X seconds after the next map load starts.   This keeps people with CPU-bound games from pinging out while their game is loading.
            SetIgnorePings(config.ignoreSecondsAfterMapChange);
        }


        public void SGDHeartBeat()
        {
            DebugMessage(3, "SGDHeartBeat()");
            foreach (SGDPlayerInfo info in playerMap.Values)
            {
                info.TimedCheck();
            }
        }


        public void DebugMessage(int level, string strMessage)
        {
            if (level <= config.debugLevel)
            {
                this.ExecuteCommand("procon.protected.pluginconsole.write", "^bSIN Game Defender: " + strMessage);
            }
        }


        private SGDPlayerInfo GetOrAddPlayerInfo(string name)
        {
            if (playerMap.ContainsKey(name))
            {
                return playerMap[name];
            }
            else
            {
                SGDPlayerInfo info = new SGDPlayerInfo(this, config, name);
                playerMap.Add(name, info);
                DebugMessage(2, "Soldier " + name + " added!");
                return info;
            }
        }


        private void RemoveSoldier(string soldierName)
        {
            playerMap.Remove(soldierName);
            DebugMessage(2, "Soldier " + soldierName + " removed!");
        }


        private void SetIgnorePings(int seconds)
        {
            DateTime time = DateTime.Now.AddSeconds(seconds);
            DebugMessage(3, "SetIgnorePings(" + time.ToString() + ")");

            foreach (SGDPlayerInfo info in playerMap.Values)
            {
                info.collectAfter = time;
            }
        }
    }
}